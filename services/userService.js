//URL base
let urlBase = 'https://jsonplaceholder.typicode.com/';

//Request API
const request = require('request');

module.exports = {

    // GET - Listar todos os usuários
    getAllUser: function  (req, res) {
        request(urlBase + 'users', {json:true},(err, resp, body)=>{
            if(err){
                res.status(404).json({"msg":"Failed to load data."});
            }
            else{
                res.status(200).json(body);
            }
        });
    },

    // GET - Listar todos os sites dos usuários
    getAllUserSite: function  (req, res) {
        let webList = [];
        request(urlBase + 'users', {json:true},(err, resp, body)=>{
            if(err){
                res.status(404).json({"msg":"Failed to load data."});
            }
            else{
                body.forEach(ws => {
                    webList.push(ws.website);
                });
                res.status(200).json(webList);
            }
        });
    },

    // GET - Listar todos usuário filtrando: NOME + EMAIL + EMPRESA
    getAllUserInfo: function  (req, res) {
        let userListInfo = [];
        request(urlBase + 'users', {json:true},(err, resp, body)=>{
            if(err){
                res.status(404).json({"msg":"Failed to load data."});
            }
            else{
                body.forEach(userInfo => {

                    let objUser = {
                        name: null,
                        mail: null,
                        company: null
                    }

                    objUser.name = userInfo.name
                    objUser.mail = userInfo.email
                    objUser.company = userInfo.company

                    userListInfo.push(objUser);
                });

                //Order array (alphabetically)
                userListInfo = userListInfo.sort((a, b) => a.name.localeCompare(b.name));

                res.status(200).json(userListInfo);
            }
        });
    },

    // GET - Listar todos usuário filtrando por: SUITE
    getAllUserSuite: function  (req, res) {
        let userListSuite = [];
        request(urlBase + 'users', {json:true},(err, resp, body)=>{
            if(err){
                res.status(404).json({"msg":"Failed to load data."});
            }
            else{
                body.forEach(suite => {
                    if(suite.address.suite.includes('Suite')){
                        //console.log('Suite -> ', suite.address.suite);
                        userListSuite.push(suite);
                    }
                });
                res.status(200).json(userListSuite);
            }
        });
    },
    
}