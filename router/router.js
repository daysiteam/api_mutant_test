
var userController = require('../services/userService');

// O arquivo de rotas importa o server node e exporta o modulo com as funções/requisições HTTP/HTTPS
module.exports = function (server){
    server.get('/users', userController.getAllUser);
    server.get('/users/web-site', userController.getAllUserSite);
    server.get('/users/infos', userController.getAllUserInfo);
    server.get('/users/suite', userController.getAllUserSuite);
}