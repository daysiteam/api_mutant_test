// Variáveis de inicialização do servidor
var express = require('express');
var server = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var router = require('./router/router');
const helmet = require('helmet');
var fs = require('fs');

// Inicializa servidor
initializeServer();

/**
 * Método para inicialização do servidor node
 */
function initializeServer() {
    // Ao inicializar o servidor adiciona os middlewares + os arquivos de rotas
    addMidlleware(server);
    router(server);
    
    // A porta você pode definir a qual você quiser
    server.listen(8080, function (req, res) {
        console.log('**** --Mutant Test API-- ****');
    });

    server.get('/', function(req, res) {
        res.sendFile(__dirname + '/htmlFiles/index.html');
    });
}

/**
 * Método que adiciona middleware no servidor node
 */
function addMidlleware() {
    server.use(cors()); //=> Adicionando CORS para liberar acessar p/ todas as requisições HTTP/HTTPS
    server.use(bodyParser.json({ limit: '512mb', extended: true })); //=> Parse para as requisições JSON e liberação de requisição com tráfego de arquivos de até 512MB

    // Configuração de segurança básica HTTP/HTTPS
    server.disable('x-powered-by');
    server.use(helmet());
    server.use(helmet.expectCt());
    server.use(helmet.permittedCrossDomainPolicies());
    server.use(helmet.referrerPolicy());
}